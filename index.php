<?php
$people = '{"data":[{"first_name":"aaron","last_name":"heath","age":34,"email":"aaron.heath@dps.com.au","secret":"VXNlIHRoaXMgc2VjcmV0IHBocmFzZSBzb21ld2hlaaagaW4geW91ciBjb2RlJ3MgY29tbWVudHM="},{"first_name":"michelle","last_name":"beech","age":21,"email": "michelle.beech@dps.com.au","secret":"YWxidXF1ZXJxdWZzIHNub3JrZWwu"}]}';
$array_of_people = (array)json_decode($people);

//a comma-separated list of email addresses
$first_variable = implode(",", array_column($array_of_people['data'], "email"));

//the original data, sorted by age descending, with a new field on each record
// called "name" which is the first and last name joined.
foreach($array_of_people['data'] as $key=>$value){
    $array_of_people['data'][$key]->name = $value->first_name." ".$value->last_name;
}

//the json you provided is already in descending order by Age but this is code for sorting by descending order for refrence
usort($array_of_people['data'],function($first,$second){
    return $first->age < $second->age;
});
$second_variable = json_encode($array_of_people);
?>